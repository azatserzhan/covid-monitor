package kz.covid.common.base

interface MvpPresenter<V : MvpView> {

    fun attach(view: V)

    fun detach()
}