package com.example.covidmonitor.base

import org.koin.core.module.Module

interface InjectionModule {
    fun create(): Module
}