package com.example.covidmonitor.base

interface MvpPresenter<V : MvpView> {

    fun attach(view: V)

    fun detach()
}