package com.example.covidmonitor

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //listener for bottom_navigation
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {item ->
        when (item.itemId){
            R.id.nav_graph -> {
                replaceFragment(GraphFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_news -> {
                replaceFragment(NewsFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_info -> {
                replaceFragment(InfoFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_about -> {
                replaceFragment(AboutFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        new comment

        //switching in bottom menu
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        //Statistics as a homepage
        replaceFragment(GraphFragment())

    }

    //switching between fragments
    private fun replaceFragment (fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container,fragment)
        fragmentTransaction.commit()
    }


}
